﻿using NUnit.Framework;
using System;
using BowlingGame;

namespace BowlingGameTest

{
    [TestFixture()]
    public class Test
    {
        [Test()]
        public void CreateGame()
        {
            var game = new Game();
        }

        [Test]
        public void RollGutterGame()
        {
            var game = new Game();
            for(int i = 0, i < 20 , i++ )
            {
                game.Roll(0);
            }

            Assert.That(game.Score(), Is.EqualTo(0));
        }

        [Test]
        public void RollOnes()
        {
            var game = new Game();
            for(int i = 0, i < 20 , i++)
            {
                game.Roll(1);
            }

            Assert.That(game.Score(), Is.EqualTo(20));
        }
    }
}
